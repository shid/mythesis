#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>


static char *uriencode_map[256];// 2^8 = 256 [100,20,15,0,0,30]
static char uriencode_str[768];

void uriencode_init(void) {
    int x;
    char *str = uriencode_str;
    for (x = 0; x < 256; x++) {
        if (isalnum(x) || x == '-' || x == '.' || x == '_' || x == '~') {
            uriencode_map[x] = NULL;
        } else {
            snprintf(str, 4, "%%%02hhX", (unsigned char)x);
            uriencode_map[x] = str;
            str += 3; /* lobbing off the \0 is fine */
        }
    }
}

bool uriencode(const char *src, char *dst, const size_t srclen, const size_t dstlen) {
    int x;
    size_t d = 0; 
    for (x = 0; x < srclen; x++) {
        if (d + 4 > dstlen)
            return false;
        if (uriencode_map[(unsigned char) src[x]] != NULL) {
            memcpy(&dst[d], uriencode_map[(unsigned char) src[x]], 3);
            d += 3;
        } else {
            dst[d] = src[x];
            d++;
        }
    }
    dst[d] = '\0';
    return true;
}

int main()
{
    uriencode_init();
    size_t srclen = nondet_uint();
    __ESBMC_assume(srclen <= 256);
    char src[srclen];
    if(srclen > 0)
    {
        for ( int i=0; i < srclen-1 ; i++ )
        {
            src[i] = nondet_char();
        }
        src[srclen - 1] = '\0';
    }
    
    size_t dstlen = nondet_uint();
    __ESBMC_assume(dstlen > 0);
    char dst[dstlen];
    uriencode(src,dst, srclen,dstlen);
    return 0;
}