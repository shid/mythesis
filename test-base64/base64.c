#include <stddef.h>
#include <assert.h>
#include <string.h>
#include "base64.h"
static const unsigned char base64_table[65] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/* Original decode function generated the table every time. I used the code to
 * print this table and pre-generate it instead.
 */
static const unsigned char dtable[256] = {
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
62, 128, 128, 128, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60,
61, 128, 128, 128, 0, 128, 128, 128, 0, 1, 2, 3, 4, 5,
6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
20, 21, 22, 23, 24, 25, 128, 128, 128, 128, 128, 128, 26, 27,
28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128
};

/**
 * base64_encode - Base64 encode
 * @src: Data to be encoded
 * @len: Length of the data to be encoded
 * @out: output uffer
 * @out_len: length of output buffer
 * Returns: Number of actual bytes encoded into the buffer
 * or 0 on failure
 *
 * Output buffer is nul terminated to make it easier to use as a C string.
 * The nul terminator is * not included in the return length.
 */
size_t base64_encode(const unsigned char *src, size_t len,
                  unsigned char *out, size_t out_len)
{
    unsigned char *pos;
    const unsigned char *end, *in;
    size_t olen;

    olen = len * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
    olen += olen / 72; /* line feeds */
    olen++; /* nul termination */
    if (olen < len) {
        return 0; /* integer overflow */
    }
    if (olen > out_len) {
        return 0; /* not enough space in output buffer */
    }
    if (out == NULL) {
        return 0;
    }

    end = src + len;
    in = src;
    pos = out;
    while (end - in >= 3) {
        *pos++ = base64_table[in[0] >> 2];
        *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
        *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
        *pos++ = base64_table[in[2] & 0x3f];
        in += 3;
    }
    if (end - in) {
        *pos++ = base64_table[in[0] >> 2];
        if (end - in == 1) {
            *pos++ = base64_table[(in[0] & 0x03) << 4];
            *pos++ = '=';
        } else {
            *pos++ = base64_table[((in[0] & 0x03) << 4) |
                          (in[1] >> 4)];
            *pos++ = base64_table[(in[1] & 0x0f) << 2];
        }
        *pos++ = '=';
    }

    *pos = '\0';
    return pos - out;
}
int main()
{
    size_t srclen = nondet_uint();
    size_t srclength = srclen;
    __ESBMC_assume(srclen >= 0 && srclen <= 256);
    __ESBMC_assume(srclength >= 0 && srclength <= 256);
    unsigned char src[srclength];
    if(srclength > 0)
    {
        for ( int i=0; i < srclength-1 ; i++ )
        {
            src[i] = nondet_uchar();
        }
        src[srclength - 1] = '\0';
        assert (src[strlen(src)] == '\0');
    }
    
    size_t olen = srclen * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
    olen += olen / 72; /* line feeds */
    olen++;
    size_t outlen = nondet_uint();
    __ESBMC_assume(outlen >= olen);
    unsigned char out[outlen];
    size_t result = base64_encode(src,srclen,out,outlen);
    assert((size_t) (outlen) >= result);
    assert(result >= 0);
    return 0;
}