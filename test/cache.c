/* -*- Mode: C; tab-width: 4; c-basic-offset: 4; indent-tabs-mode: nil -*- */
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>

#ifndef NDEBUG
#include <signal.h> 
#endif

#include "cache.h"

#ifndef NDEBUG
const uint64_t redzone_pattern = 0xdeadbeefcafedeed;
int cache_error = 0;
#endif
/***** QUESTIONS
1. Generally what results must be reached
2. What to write in thesis (for example, any diagrams about state flow are needed ?)
3. Unwinding of dependency like in strdup() function in string.c
4. Mempry leak
*****/
cache_t* cache_create(const char *name, size_t bufsize, size_t align) {
    cache_t* ret = calloc(1, sizeof(cache_t));
    char* nm = malloc(strlen(name) + 1); // +1 for the null terminator
if (nm != NULL) {
    strcpy(nm, name);
}//Unwind is needed here to stop unlimited loop unwinding
    //String.c (basic class in c)
    
    if (ret == NULL || nm == NULL ||
        pthread_mutex_init(&ret->mutex, NULL) == -1) {
        free(ret);
        free(nm);
        return NULL;
    }
    assert (ret != NULL);
    assert (nm != NULL);
    assert(strcmp(nm,name) == 0);
    ret->name = nm;
    STAILQ_INIT(&ret->head);

#ifndef NDEBUG
    ret->bufsize = bufsize + 2 * sizeof(redzone_pattern);
#else
    ret->bufsize = bufsize;
#endif
    assert(ret->bufsize >= sizeof(struct cache_free_s));

    return ret;
}
void cache_set_limit(cache_t *cache, int limit) {
    pthread_mutex_lock(&cache->mutex);
    cache->limit = limit;
    pthread_mutex_unlock(&cache->mutex);
}

void main()
{
    size_t x = 100; 
    //x = nondet_uint(); // Generate a non-deterministic unsigned integer
    char s[x+1];

    for ( int i=0; i<x ; i++ )
    {
        s[i] = nondet_char();
    }
   s[x] = '\0';
    size_t bufsize = (size_t)nondet_uint();
    size_t align = (size_t)nondet_uint();

    cache_t* my_cache1 = cache_create(s,bufsize,align);
    // cache_t* my_cache2 = cache_create("John",bufsize,align);
    // pthread_t id1, id2;
    // pthread_create(&id1, NULL, cache_set_limit, NULL);
    // pthread_create(&id2, NULL, cache_set_limit, NULL);
    // pthread_join(id1, NULL);
    // pthread_join(id2, NULL);



   
    
    free(my_cache1);

    
    


    
}
